Welcome to MIE (Make It Easy)
-----------------------------

### Introduction

MIE is a markdown file CMS. It helps to convert your flat files, which are written using [Markdown](http://daringfireball.net/projects/markdown/syntax), to beautiful web pages.

If you create a folder within the content folder (e.g. `public/content/sub`) and put an `index.md` inside it, you can access that folder at the URL 
`http://yousite.com/sub`. If you want another page within the sub folder, simply create a text file with the corresponding name (e.g. `public/content/sub/page.md`)
and you will be able to access it from the URL `http://yousite.com/sub/page`. 

If a file cannot be found, the file `public/theme/errors/html/error_404.php` will be shown.

### Text File Markup

Text files are marked up using [Markdown](http://daringfireball.net/projects/markdown/syntax). 

At the top of text files you can place a block comment and specify certain attributes of the page. For example:

```
/*
Title: Welcome
Description: Will go in the meta description tag
Author: Liu Tao
Date: 2014/02/27
Robots: noindex,nofollow
*/
```

### Documentation

MIE was inspired by Pico, and has been rewritten with CodeIgniter 3, which is super fast and with high scalability.

For more help have a look at the MIE documentation at [MIE Wiki](https://bitbucket.org/ahgigu/mie/wiki)