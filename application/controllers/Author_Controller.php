<?php
/**
 * MIE
 *
 * An open source Markdown CMS for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * @package		MIE
 * @author		Liu Tao(ahgigu)
 * @copyright	Copyright (c) 2014. (http://mie.ahgigu.com)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://mie.ahgigu.com
 * @since		Version 1.1
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Author_Controller extends MIE_Controller {

	/**
	 * Constructor.
	 *
	 */
    public function __construct() {

        parent::__construct();

        if (($this->action != 'login') && ($this->action != 'auth')) {
            if (!($this->is_author())) {
                redirect (AUTHOR_URI . '/login');
            }
        } else {
            if ($this->is_author()) {
                redirect (AUTHOR_URI);
            }
        }
    }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {

        $ty1 = '';
        $ty2 = '';
        $filesize = '';
        $filetime = '';
        $files = array();
        $dirs = array();
        $active_path = $this->input->get('path', TRUE);
        $path = CONTENTPATH . $active_path;

        if (file_exists($path)) {
            $dh = dir($path);
            $this->load->helper(array('date','number'));
            while(($file = $dh->read()) !== FALSE) {
                if($file!="." && $file!=".." && !is_dir("$path/$file")){
                    $filesize = filesize("$path/$file");
                    $filesize = byte_format($filesize, 2);
                    $filetime = filemtime("$path/$file");
                    $filetime = date('Y-m-d H:i:s', local_to_gmt($filetime));

                } else {
                    $filesize = '';
                    $filetime = '';
                }

                if($file == ".") {
                    continue;
                } else if($file == "..") {
                    if(empty($active_path)) {
                        continue;
                    }
                } else {
                    $file_self = array();
                    $file_self['name'] = $file;
                    if(is_dir("$path/$file")) {
                        $dirs[] = $file_self;
                    } else {
                        $file_self['size'] = $filesize;
                        $file_self['time'] = $filetime;
                        $files[] = $file_self;
                    }
                }
            }
        }

        $this->view_vars['page_title'] = 'Files';
        $this->view_vars['dirs'] = $dirs;
        $this->view_vars['files'] = $files;
        $this->view_vars['active_path'] = $active_path;

        $this->view_vars['partial'] = 'author/files';
        $this->load->view('author/author_layout', $this->view_vars);
	}

    /**
     * Create new directory.
     *
     * @return null
     */	
    public function new_folder()
    {
        
        $this->load->helper(array('url', 'form'));

        $active_path = $this->input->get('path', TRUE);

        $this->view_vars['page_title'] = 'Create New Directory';
        $this->view_vars['active_path'] = $active_path;
        $this->view_vars['partial'] = 'author/new_folder';
        $this->load->view('author/author_layout', $this->view_vars);
    }

   /**
     * Do create dir action
     *
     * @return null
     */	
    public function do_new_folder()
    {
        $this->load->helper(array('url', 'form'));
		$this->load->library('form_validation');

		$this->form_validation->set_rules('name', 'Folder Name', 'trim|required|alpha_dash_dot');

		if ($this->form_validation->run() === FALSE)
		{
            $this->new_folder();
		} else {
            $active_path = $this->input->post('path', TRUE);
            $file_name = $this->input->post('name', TRUE);
            $path = CONTENTPATH . $active_path;
            $dir_name = $path . '/' . $file_name;
            $result = FALSE;

            if(is_really_writable($path) && (!file_exists($dir_name)))
            {
                $result = mkdir($dir_name, 0777);
            }
            if($result) {
                $this->session->set_flashdata('new_dir_success_msg', "Directory <em>{$file_name}</em> has been created.");
            } else {
                $this->session->set_flashdata('new_dir_failure_msg', "<em>{$file_name}</em> exists already or you are not permitted to create diretory in this path.");
            }

            $_dest = AUTHOR_URI . (isset($active_path[0]) ? '?path='. urlencode($active_path) : '');

            redirect($_dest);
		}
    }

    /**
     * Upload files.
     *
     * @return null
     */	
    public function do_upload()
    {
        $path = $this->input->get('path', TRUE);
        $upload_path = CONTENTPATH . $path;
        if(is_dir($upload_path) && is_really_writable($upload_path)) {
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'md|zip|gif|jpg|png|xls|xlsx|doc|docx|txt|ppt|pptx';
            $config['max_size']	= 10*1024;
            $this->load->library('upload', $config);

            $this->upload->do_upload('file');
        }
    }


   /**
     * Delete dirs or files.
     *
     * @return null
     */	
    public function delete()
    {
        
        $this->load->helper(array('url', 'form'));

        $active_path = $this->input->get('path', TRUE);
        $file_names = $this->input->get('name', TRUE);

        $this->view_vars['page_title'] = 'Delete';
        $this->view_vars['active_path'] = $active_path;
        $this->view_vars['file_names'] = $file_names;
        $this->view_vars['partial'] = 'author/delete';
        $this->load->view('author/author_layout', $this->view_vars);
    }

    /**
     * Do delete dirs or files action.
     *
     * @return null
     */	
    public function do_delete()
    {
        $this->load->helper(array('url', 'file'));
        $result = FALSE;
        $active_path = $this->input->post('path', TRUE);
        $file_names = $this->input->post('name', TRUE);

        foreach ($file_names as $key=>$file_name) {
            $path = rtrim(CONTENTPATH, '/');
            $path .= $active_path . '/' . $file_name;
            //echo '<pre>';
            //var_dump ($path);
            //exit;
            if(is_really_writable($path)) {
                if(is_file($path)){
                    $result = unlink($path);
                } else {
                    $result = delete_files($path, TRUE, FALSE, 1);
                }
            }
        }
        if($result) {
            $this->session->set_flashdata('del_success_msg', "<em>{$file_name}</em> has been deleted.");
        } else {
            $this->session->set_flashdata('del_failure_msg', "<em>{$file_name}</em> does not exit anymore or you are not permitted to delete this file/directory.");
        }

        $_dest = AUTHOR_URI . (isset($active_path[0]) ? '?path='. urlencode($active_path) : '');
        redirect($_dest);
    }
    
    /**
     * Download all userdata.
     *
     * @return null
     */	
    public function download()
    {
        $this->load->library('zip');

        $this->zip->read_dir(PUBLICDIR);

        $this->zip->archive('my_backup.zip'); 

        $this->zip->download('my_backup.zip');
        
        $this->zip->clear_data();
    }
	
	/**
     * login page.
     *
     * @return null
     */	
    public function login()
    {
        
        $this->load->helper(array('url', 'form'));

        $this->view_vars['page_title'] = 'Welcome back';

        $this->load->view('author/login', $this->view_vars);
    }

	/**
     * Do login.
     *
     * @return null
     */	
    public function auth(){
		$this->load->library('form_validation');

		$this->form_validation->set_rules('un', 'User Name', 'trim|required');
		$this->form_validation->set_rules('up', 'Password', 'trim|required');

		if ($this->form_validation->run() === TRUE) {
            $user_name = $this->input->post('un', TRUE);
            $user_password = $this->input->post('up', TRUE);
            $user = $this->user->validate_user($user_name, $user_password);
            if(isset($user)){
                $this->session->set_userdata('name', $user['name']);
                redirect(AUTHOR_URI);
            } else {
                $this->session->set_flashdata('auth_fail_msg', "Unrecognized account or password.");
                redirect(AUTHOR_URI . "/login");
            }
        } else {
            $this->login();
        }
    }

    public function logout(){
        $this->session->unset_userdata('name');
        redirect(AUTHOR_URI . '/login');
    }
}
/* End of file Author_Controller.php */
/* Location: ./application/controllers/Author_Controller.php */
