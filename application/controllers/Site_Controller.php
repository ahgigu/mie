<?php
/**
 * MIE
 *
 * An open source Markdown CMS for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * @package		MIE
 * @author		Liu Tao(ahgigu)
 * @copyright	Copyright (c) 2014. (http://mie.ahgigu.com)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://mie.ahgigu.com
 * @since		Version 1.1
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_Controller extends MIE_Controller {

	/**
	 * Constructor.
	 *
	 */
    public function __construct() {
        parent::__construct();
    }

	/**
	 * Index Page for this controller.
	 *
	 */
	public function index() {

        $uri = $this->uri->uri_string();

        // Get the file path
		if(!empty($uri[0])) {
            $file = CONTENTPATH . $uri . CONTENTEXT;
            if(file_exists($file) && !is_dir($file)){
                $content = file_get_contents($file);
            } else {
                show_404();
            }
        }

		$pages = $this->get_pages();
        $lastest_md = $pages[0];
        $is_front = FALSE;
        $front_path = $lastest_md['path'];
        //Home page
        if(empty($uri[0])){
            $file = $lastest_md['path'];
            $content = file_get_contents($file);
            $is_front = TRUE;
            $index_meta = array(
                'description' => SITE_DESC,
                'keywords' => SITE_KEYWORDS
            );
        }

        //Current md
		$meta = $this->read_file_meta($content);
        $content = $this->parse_content($content);
        
        //All mds for nav
		$prev_page = array();
		$current_page = array();
		$next_page = array();
        $count = count($pages);
        for ($i = 0; $i < $count; $i++) {
		    $_page = $pages[$i];
			if((isset($file)) && ($file == $_page['path'])){
                $current_page = $_page;
                if(($i-1) > -1) $prev_page = $pages[$i-1]; 
                if(($i+1) < $count) $next_page = $pages[$i+1]; 
				break;
			}

        }

        $packages_raw = array();
        $packages = array();
        foreach($pages as $page){
            $packages_raw[] = $page['package'];
        }
        $packages_raw = array_unique($packages_raw, SORT_STRING);
        foreach ($packages_raw as $package) {
            $pack = array();
            $pack['path'] = $package;
            $pack['display'] = $package;
            $pack['display'] = str_replace('_', ' ', $pack['display']);
            $packages[] = $pack;
        }
        // Obtain a list of columns
        foreach ($packages as $key => $row) {
            $display_cmp[$key]  = $row['path'];
        }
        // Sort the data with path asc
        array_multisort($display_cmp, SORT_ASC, $packages);
        $packages = array_values($packages);

		// Variables will be retrived from template page.
        $page_vars = array(
            'site_title' => SITE_TITLE,
			'meta' => $is_front ? $index_meta : $meta,
			'content' => $content,
            'packages' => $packages,
			'pages' => $pages,
			'prev_page' => $prev_page,
			'next_page' => $next_page,
            'uri' => $uri,
            'is_front_page' => $is_front,
            'front_path' => $front_path
        );

		$this->load->view('template', $page_vars);
	}

	/**
	 * Parses the content using Parsedown
	 *
	 * @param string $content the raw txt content
	 * @return string $content the Markdown formatted content
	 */
	protected function parse_content($content)
	{
        $this->load->library('Parsedown');
        
        $base_url = site_url();
        $base_url = rtrim($base_url, '/\\');
		$content = preg_replace('#/\*.+?\*/#s', '', $content, 1); // Remove comments and meta
		$content = str_replace('%base_url%', $base_url, $content);
        
        $content = Parsedown::instance()->parse($content);

		return $content;
	}

    /**
	 * Parses the file meta from the txt file header
	 *
	 * @param string $content the raw txt content
	 * @return array $headers an array of meta values
	 */
	protected function read_file_meta($content)
	{
		$headers = array(
			'title'       	=> 'Title',
			'description' 	=> 'Description',
			'author' 		=> 'Author',
			'date' 			=> 'Date',
			'robots'     	=> 'Robots',
			'template'      => 'Template'
		);

	 	foreach ($headers as $field => $regex){
            $closing = strpos($content, '*/');
            $content = substr($content, 0, ($closing+2));

			if (preg_match('/^[ \t\/*#@]*' . preg_quote($regex, '/') . ':(.*)$/mi', $content, $match) && !empty($match[1])){
				$headers[ $field ] = trim(preg_replace("/\s*(?:\*\/|\?>).*/", '', $match[1]));
			} else {
				$headers[ $field ] = '';
			}
		}

		if(isset($headers['date'])) $headers['date_formatted'] = date(DATEFORMATE, strtotime($headers['date']));

		return $headers;
	}

    /**
	 * Get a list of pages
	 *
	 * @return array $sorted_pages an array of pages
	 */
	protected function get_pages()
	{

        $base_url = site_url();
        $base_url = rtrim($base_url, '/\\');

		$pages = $this->get_files(CONTENTPATH, CONTENTEXT);
		$sorted_pages = array();
		foreach($pages as $key=>$page){

			// Get title and format $page
			$page_content = file_get_contents($page);
			$page_meta = $this->read_file_meta($page_content);
			$url = str_replace(CONTENTPATH, $base_url .'/', $page);
			$url = str_replace(CONTENTEXT, '', $url);
            $uri = str_replace($base_url .'/', '', $url);

            $package = '';
            if (strpos($uri,'/') !== FALSE) {
                $packages = explode('/', $uri);
                $package = $packages[0];
            }

			$data = array(
                'package' => $package,
                'path' => $page,
				'title' => isset($page_meta['title']) ? $page_meta['title'] : '',
				'uri' => $uri,
				'url' => $url,
				'author' => isset($page_meta['author']) ? $page_meta['author'] : '',
				'date' => isset($page_meta['date']) ? $page_meta['date'] : '',
				'date_formatted' => isset($page_meta['date']) ? date(DATEFORMATE, strtotime($page_meta['date'])) : ''
			);

			if(!empty($page_meta['date'])) {
				$sorted_pages[] = $data;
            }
		}

        // Obtain a list of columns
        foreach ($sorted_pages as $key => $row) {
            $date_cmp[$key]  = $row['date'];
        }
        // Sort the data with date descending
        array_multisort($date_cmp, SORT_DESC, $sorted_pages);

        $sorted_pages = array_values($sorted_pages);

		return $sorted_pages;
	}

	/**
	 * Helper function to recusively get all files in a directory
	 *
	 * @param string $directory start directory
	 * @param string $ext optional limit to file extensions
	 * @return array the matched files
	 */ 
	protected function get_files($directory, $ext = '')
	{
	    $array_items = array();
	    if($handle = opendir($directory)){
	        while(FALSE !== ($file = readdir($handle))){
	            if(preg_match("/^(^\.)/", $file) === 0){
	                if(is_dir($directory. "/" . $file)){
	                    $array_items = array_merge($array_items, $this->get_files($directory. "/" . $file, $ext));
	                } else {
	                    $file = $directory . "/" . $file;
	                    if(!$ext || strstr($file, $ext)) $array_items[] = preg_replace("/\/\//si", "/", $file);
	                }
	            }
	        }
	        closedir($handle);
	    }

	    return $array_items;
	}
}
/* End of file Site_Controller.php */
/* Location: ./application/controllers/Site_Controller.php */
