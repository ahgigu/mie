<?php
class User extends MIE_Model {

	public function __construct() {
        parent::__construct();
        $this->set_table_name('user');
	}

	public function find_users($slug = FALSE) {
        $query_sql = "select * from user";
		$query = $this->db->query($query_sql);
		$users = $query->result_array();
		return $users;
	}

	public function find_by_email($email) {
        $this->db->where('email', $email);
        $query = $this->db->get('user');
        return $query->row_array();
	}

    public function validate_user($user_name, $user_password) {
        $user = $this->find_by_email($user_name);
        
        if(!empty($user) && $user['pass'] === $user_password){
            //$this->load->library('phpass');
            //return $this->phpass->check($user_password, $user['user_pass']) ? $user : NULL;
            return $user;
        } else {
            return NULL;
        }
    }
}
