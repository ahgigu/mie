<?php
/**
 * MIE
 *
 * An open source Markdown CMS for PHP 5.2.4 or newer
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Academic Free License version 3.0
 *
 * @package		MIE
 * @author		Liu Tao(ahgigu)
 * @copyright	Copyright (c) 2014. (http://mie.ahgigu.com)
 * @license		http://opensource.org/licenses/AFL-3.0 Academic Free License (AFL 3.0)
 * @link		http://mie.ahgigu.com
 * @since		Version 1.1
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class MIE_Controller extends CI_Controller {

    protected $controller = '';
    protected $action = '';
    protected $view_vars = array();

	/**
	 * Constructor.
	 *
	 */
    public function __construct() {

        parent::__construct();

        $this->controller = $this->router->fetch_class();
        $this->action = $this->router->fetch_method();
        $this->_set_view_vars();

        $this->load->helper(array('url'));
        $this->load->model(array('user'));
    }

    protected function is_author() {
        $user_name = $this->session->userdata('name'); 
        if (isset($user_name)) {
            return TRUE;
        }
        return FALSE;
    }

    protected function _set_view_vars() {
        $this->view_vars['controller'] = $this->controller;
        $this->view_vars['action'] = $this->action;
    }

}
// END MIE_Controller class

/* End of file MIE_Controller.php */
/* Location: ./application/core/MIE_Controller.php */
