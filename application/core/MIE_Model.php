<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * HakkaBiz
 *
 * An open source CMS for SEO and e-commerce
 *
 * PHP version 5.1.2+
 *
 * @category  PHP
 * @package   HakkaBiz
 * @author    Ahgigu <ahgigu@ahgigu.com>
 * @copyright 2013-2014 Ahgigu.com
 * @license   http://ahgigu.com/hakka/licence.html BSD Licence
 * @version   Release: 0.1.2
 * @link      http://ahgigu.com/hakka/
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * HB Model Class
 *
 * @package     HB		
 * @subpackage	Libraries
 * @category	Libraries
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/libraries/config.html
 */
class MIE_Model extends CI_Model {

    /**
     * The name of the associated table name of the Model object
     *
     * @var string
     * @access private
     */
    private $_table = '';

    /**
     * The name of the primary key field for this Model
     *
     * @var string
     * @access private
     */
    private $_primary_key = 'id';

    /**
     * Constructor
     *
     * @access public
     */
    function __construct()
    {
        parent::__construct();

        $this->load->database();

        log_message('debug', "MIE Model Class Initialized");
    }

	/**
	 * get_table_name
	 *
	 * Get the associated database table name
	 *
	 * @access public
	 */
    public function get_table_name()
    {
        return $this->_table;
    }

	/**
	 * set_table_name
	 *
	 * Set the associated database table name
	 *
	 * @param  string
	 * @access protected
	 */
    protected function set_table_name($table_name)
    {
        $this->_table = $table_name;
    }

	/**
	 * get_primary_key
	 *
	 * Get the associated database table primary key.
	 *
	 * @access public
	 */
    protected function get_primary_key()
    {
        return $this->_primary_key;
    }

	/**
	 * set_primary_key
	 *
	 * Set the associated database table primary key.
	 *
	 * @param  string
	 * @access protected
	 */
    protected function set_primary_key($key_name)
    {
        $this->_primary_key = $key_name;
    }

    /**
     * Find data by id
     *
     * @return model data.
     */
    public function find_by_id($id)
    {
        return $this->db->limit(1)->get_where($this->_table, array($this->_primary_key=>$id))->row_array();
    }
}
/* End of file MIE_Model.php */
/* Location: ./application/core/MIE_Model.php */
