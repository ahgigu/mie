<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * MIE
 *
 * An open source CMS for SEO and e-commerce
 *
 * PHP version 5.1.6+
 *
 * @category  PHP
 * @package   MIE
 * @author    Ahgigu <ahgigu@ahgigu.com>
 * @copyright 2013-2014 Ahgigu.com
 * @license   http://ahgigu.com/mie/licence.html BSD Licence
 * @version   Release: 0.1.2
 * @link      http://ahgigu.com/mie/
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * MIE Form Validation Class
 *
 * @package		MIE
 * @subpackage	Libraries
 * @category	Validation
 * @author    Ahgigu <ahgigu@ahgigu.com>
 * @copyright 2013-2014 Ahgigu.com
 * @license   http://ahgigu.com/mie/licence.html BSD Licence
 * @version   Release: 0.1.2
 * @link      http://ahgigu.com/mie/
 */
class MIE_Form_validation extends CI_Form_validation {

	/**
	 * Constructor
	 */
	public function __construct($rules = array())
	{
        parent::__construct($rules);

		log_message('debug', "MIE Form Validation Class Initialized");
	}

	// --------------------------------------------------------------------

	/**
	 * Alpha-numeric with underscores, dashes and dots
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function alpha_dash_dot($str)
	{
		return ( ! preg_match("/^([-a-z0-9._-])+$/i", $str)) ? FALSE : TRUE;
	}

}
// END MIE Form Validation Class

/* End of file MIE_Form_validation.php */
/* Location: ./application/libraries/MIE_Form_validation.php */