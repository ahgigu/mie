/*
Title: Linux Tips
Description: Linux tips that turn long jobs into a moment's work.
Keywords: linux hacks, linux tricks, linux tips, linux techniques
Author: Liu Tao
Date: 2014/03/13
*/

Linux Tips
==================================================

### Processes not Run by You

```
ps aux | grep -v `whoami`
```

Find the Top Ten:

```
ps aux  --sort=-%cpu | grep -m 11 -v `whoami`
```


### Replacing same text in multiple files

```
perl -i -pe 's/Windows/Linux/;' test*
```

To replace the text `Windows` with `Linux` in all text files in current directory and down:

```
find . -name '*.txt' -print | xargs perl -pi -e's/Windows/Linux/ig' *.txt
```

```
find -type f -name '*.txt' -print0 | xargs --null perl -pi -e 's/Windows/Linux/'
```


### Backup your website easily

```
rsync -vare ssh jono@192.168.0.2:/home/jono/importantfiles/* /home/jono/backup/
```


### Keeping your clock in time

```
ntpdate ntp.blueyonder.co.uk
```
A list of suitable NTP servers is available at www.eecis.udel.edu/~mills/ntp/clock1b.html


### Finding the biggest files

```
ls -lSrh
```

```
ls -lSrh *.mp*
```

To  look for the largest directories:

```
du -kx | egrep -v "\./.+/" | sort -n
```

### Listing today's files only

```
ls -al --time-style=+%D | grep `date +%D`
```

### Editing without an editor

To print columns eg 1 and 3 from a file file1 into file2

```
awk '{print $1, $3}' file1 > file2
```

To output only characters from column 8 to column 15 of file1

```
cut -c 8-15 file1 > file2
```

To replace the word word1 with the word word2 in the file file1

```
sed "s/word1/word2/g" file1 > file2
```

To merge columns in files

```
#!/bin/sh
length=`wc -l $1 | awk '{print $1}'`
count=1
[ -f $3 ] && echo "Optionally removing $3" && rm -i $3
while [ "$count" -le "$length" ] ; do
      a=`head -$count $1 | tail -1`
      b=`head -$count $2 | tail -1`
      echo "$a      $b" >> $3
      count=`expr $count + 1`
done
```

```
/path/to/merge.sh file1 file2 file3
```


### Backup selected files only

To use tar to backup only certain files in a directory

```
cat >> /etc/backup.conf
# /etc/passwd
# /etc/shadow
# /etc/yp.conf
# /etc/sysctl.conf
EOF
```
Then run tar with the -T flag pointing to the file just created
```
tar -cjf bck-etc-`date +%Y-%m-%d`.tar.bz2 -T /etc/backup.conf
```


###  Case sensitivity

To change the case of a bunch of files

```
#!/bin/sh
for i in `ls -1`; do
        file1=`echo $i | tr [A-Z] [a-z] `
        mv $i $file1 2>/dev/null
done
```

### Find

```
find . -type f -print0 | xargs -0 grep -l ".png"
```

### Add User to group

Existing user.
```
usermod -a -G groupName userName
groups userName
```

Add a new user and add to an existing group
```
useradd -G groupName username
```

or need to create a new group
```
groupadd groupName
```

## See also

* [http://www.tuxradar.com/content/linux-tips-every-geek-should-know](http://www.tuxradar.com/content/linux-tips-every-geek-should-know)