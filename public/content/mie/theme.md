/*
Title: Theme
Description: Installing Nginx, Mysql, PHP and FTP in CentOS and Ubuntu.
Author: Liu Tao
*/

Install Nginx, Mysql, PHP and FTP
---------------------------------

This document provids 'one click' solutions to install Nginx, Mysql, PHP and FTP in CentOS and Ubuntu servers.

### CentOS

* Download the `installer`

```
wget --no-check-certificate https://bitbucket.org/ahgigu/lemp_installer/get/master.tar.gz
```


* Install

```
tar -zxvf master.tar.gz
chmod –R 777 sh
cd sh
tuning/network.sh
tuning/tuning.sh
./install.sh
```

* Accounts

```
cat account.log
```

* Directories

**Web files directory**: `/alidata/www`

**Web server**: `/alidata/server`

**Mysql**: `/alidata/server/mysql`

**PHP**: `/alidata/server/php`
 
**Nginx**: `/alidata/server/nginx/`
 
**Apache**: `/alidata/server/httpd`
