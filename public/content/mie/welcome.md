/*
Title: Welcome to MIE
Description: MIE (Make It Easy) is an open source flat markdown file CMS.
Author: Liu Tao
Date: 2014/02/20
*/

Welcome to MIE (Make It Easy)
-----------------------------

### Introduction

MIE(Make It Easy) is a markdown file CMS. It helps convert your flat files, which are written using Markdown, to beautiful web pages.

Below are some examples of content locations and their corresponing URL's:

<table class="table table-bordered">
	<thead>
		<tr><th>Physical Location</th><th>URL</th></tr>
	</thead>
	<tbody>
		<tr><td>public/content/sub.md</td><td>/sub</td></tr>
		<tr><td>public/content/sub/page.md</td><td>/sub/page</td></tr>
		<tr><td>public/content/a/very/long/url.md</td><td>/a/very/long/url</td></tr>
	</tbody>
</table>


If a file cannot be found, the file `public/theme/errors/html/error_404.php` will be shown.

### Text File Markup

Text files are marked up using [Markdown](http://daringfireball.net/projects/markdown/syntax). 

At the top of text files you should place a block comment and specify certain attributes of the page(only `Title` and `Date` are required). For example:

```
/*
Title: Welcome
Description: Will go in the meta description tag
Author: Liu Tao
Date: 2014/02/27
Robots: noindex,nofollow
*/
```

### Documentation

MIE has been rewritten with CodeIgniter 3, which is super fast and with high scalability.

For more help have a look at the MIE documentation at [MIE Docs](http://mie.ahgigu.com/)

### Download

Get the [latest release](https://bitbucket.org/ahgigu/mie/get/master.zip). 


