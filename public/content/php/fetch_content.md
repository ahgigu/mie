/*
Title: Fetch webpage content by URL
Description: PHP fetch web page content by providing a URL.
Author: Liu Tao
Date: 2014/04/28
*/

Fetch web page title
-----------------------------

For example:

```
    /**
     * get page title by url
     *
     * Get page title by specified url
     *
     * @param string $url specified url
     *
     * @return string
     *
     * @access private
     */
    private function _get_page_title_by_url($url)
    {
        $title = "";

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

        $_document = curl_exec($ch);
        curl_close($ch);

        $_count = preg_match("/<title>(.+)<\/title>/Usi", $_document, $matches);

        if ($_count > 0) {
            $title = $matches[1];
            $title = mb_convert_encoding($title, 'utf-8', 'utf-8, gb2312');
        }

        return empty($title) ? $url : $title;
    }

```

Get the [latest release](https://bitbucket.org/ahgigu/mie/get/master.zip). 


