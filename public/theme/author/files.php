<?php if ($_partial === 'content') { ?>
<div class="col-md-12">
    <ol class="breadcrumb">
        <li class="active">
                public
        </li>
        <li <?php echo isset($active_path[0])? '' : 'class="active"';?>>
            <?php if(isset($active_path[0])) { ?>
            <a href="<?php echo base_url(AUTHOR_URI);?>">
                content
            </a>
            <?php } else { ?>
            content
            <?php } ?>
        </li>
        <?php
            $_path_dirs = explode('/', $active_path);
            $_path_dirs = array_filter($_path_dirs);
            $_path_dirs = array_values($_path_dirs);
            $_count = count($_path_dirs);
            $_bread_path = '';
            foreach($_path_dirs as $key=>$_dir){
                if(isset($_dir[0])){
                    $_bread_path .=  '/' . $_dir;
                    if($_count == $key + 1){
        ?>
                    <li class="active">
                        <?php echo $_dir;?>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo site_url(AUTHOR_URI . '?path=') . urlencode($_bread_path);?>">
                            <?php echo $_dir;?>
                        </a>
                    </li>

        <?php }}} ?>
    </ol>

<?php 
$_new_dir_success_msg = $this->session->flashdata('new_dir_success_msg');
if(!empty($_new_dir_success_msg)) { 
?>
<div class="alert alert-success">
<?php echo $_new_dir_success_msg;?>
</div>
<?php } ?>
<?php 
$_new_dir_failure_msg = $this->session->flashdata('new_dir_failure_msg');
if(!empty($_new_dir_failure_msg)) { 
?>
<div class="alert alert-danger">
<?php echo $_new_dir_failure_msg;?>
</div>
<?php } ?>

<div id="not_selected" class="alert alert-danger" style="display:none;">Please select files or folders you want to delete.</div>

<div id="upload_errors" class="alert alert-danger" style="display:none;"></div>

<div id="upload_progress" class="progress progress-striped active" style="display:none;">
    <div class="progress-bar"  role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
        <span class="sr-only">45% Complete</span>
    </div>
</div>



    <ul class="pager">
        <li class="previous">
            <a id="delete_link" href="#">
                Delete
            </a>
        </li>
        <li class="next" id="new_file_folder">
             <a href="<?php echo site_url(AUTHOR_URI . '/new_folder'.(isset($active_path[0]) ? '?path='. urlencode($active_path) : ''));?>">
                Folder
            </a>
            <a id="upload_btn" href="#">
                Upload
            </a>
        </li>
    </ul>
    <div class="table-responsive">
    <form id="file_form" action="<?php echo site_url(AUTHOR_URI . '/delete'); ?>" method="get" accept-charset="utf-8">
        <input type="hidden" name="path" value="<?php echo $active_path; ?>">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th width="5%">
                        <input type="checkbox">
                    </th>
                    <th>
                        Name
                    </th>
                    <th width="15%">
                        Size
                    </th>
                    <th width="15%">
                        Modified
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($dirs as $_dir) {?>
                <tr>
                    <td width="5%">
                        <input type="checkbox" name="name[]" value="<?php echo $_dir['name']; ?>">
                    </td>
                    <td>
                    <?php if(preg_match("/^([-a-z0-9._-])+$/i", $_dir['name'])){ ?>
                        <a href="<?php echo site_url(AUTHOR_URI . '?path=') . urlencode($active_path.'/'.$_dir['name']); ?>"><?php echo $_dir['name']; ?></a>
                    <?php } else { ?>
                            <span style="color:red"><?php echo $_dir['name']; ?> (unavailable, please rename it)</span>
                    <?php } ?>
                    </td>
                    <td width="20%">
                    </td>
                    <td width="20%">
                    </td>
                </tr>
                <?php } ?>
                <?php foreach($files as $_file) {?>
                <?php if($_file['name'] != 'index.html') { ?>
                <tr>
                    <td width="5%">
                        <input type="checkbox" name="name[]" value="<?php echo $_file['name']; ?>">
                    </td>
                    <td>
                    <?php if(preg_match("/^([-a-z0-9._-])+$/i", $_file['name'])){ ?>
                        <?php echo $_file['name']; ?>
                    <?php } ?>
                    </td>
                    <td width="20%">
                        <?php if(isset($_file['size'][0])) { 
                            echo $_file['size'];
                        } ?>
                    </td>
                    <td width="20%">
                        <?php if(!empty($_file['time'])) { 
                            echo $_file['time'];
                        } ?>
                    </td>
                </tr>
                <?php } ?>
                <?php } ?>
            </tbody>
        </table>
    </form>
    </div>
</div>
<?php } ?>
<?php if ($_partial === 'end') { ?>
<script type="text/javascript" src="<?php echo base_url().ASSETURI; ?>/plugins/plupload210/plupload.full.min.js"></script>
<script>
$(function(){
    //checkbox
    $('table th input:checkbox').on('click' , function(){
        var that = this;
        $(this).closest('table').find('tr > td:first-child input:checkbox')
        .each(function(){
            this.checked = that.checked;
        });
    });
    //delete_link
    $('#delete_link').click(function() {
        event.preventDefault();
        var checked=$('input:checked').length;
        if(checked){
            $('#file_form').submit();
        } else {
            $('#not_selected').show();
        }
    });

    //plupload
    var uploader = new plupload.Uploader({
    runtimes : 'html5,flash,silverlight,html4',
    browse_button : 'upload_btn', // you can pass in id...
    container: document.getElementById('new_file_folder'), // ... or DOM Element itself
    url : '<?php echo site_url(AUTHOR_URI . "/do_upload") . (isset($active_path[0]) ? '?path='. urlencode($active_path) : ''); ?>',
    flash_swf_url : '<?php echo base_url(); ?>public/garland/plugins/plupload210/Moxie.swf',
    silverlight_xap_url : '<?php echo base_url(); ?>public/garland/plugins/plupload210/Moxie.xap',
    multi_selection: true,
    unique_names: true,

    filters : {
        max_file_size : '2mb',
        mime_types: [
            {title : "Image files", extensions : "jpg,gif,png"},
			{title : "Zip files", extensions : "zip"},
			{title : "Office files", extensions : "md,doc,docx,xls,xlsx,ppt,pptx,txt"}
        ]
    },

    init: {
        PostInit: function() {
        },

        FilesAdded: function(up, files) {
           // $('#upload_progress')[0].innerHTML = '';
           // plupload.each(files, function(file) {
           //     $('#upload_progress')[0].innerHTML += '<span id="' + file.id + '">' + file.name +' (' + plupload.formatSize(file.size) + ') <b></b></span><br>';
           // });
           $('#upload_progress .progress-bar').css('width', '0').attr('aria-valuenow', '0');
           $('#upload_progress .progress-bar .sr-only').html('0% Complete');
            uploader.start();
        },

        UploadProgress: function(up, file) {
           $("#upload_progress").show();
           $('#upload_progress .progress-bar').css('width', file.percent + '%').attr('aria-valuenow', file.percent);
           $('#upload_progress .progress-bar .sr-only').html(file.percent + '% Complete');
        },

        UploadComplete: function(up, file, ret){
            var current_path = window.location.href;
            //window.location.href = current_path;
        },

        Error: function(up, err) {
           // $('#upload_errors')[0].innerHTML += '<span id="' + err.file.id + '">' + err.file.name +' (' + plupload.formatSize(err.file.size) + ') <b></b></span><br>';
           // $("#upload_error #" + err.file.id + " b")[0].innerHTML += " Error: " +  err.message;
           $("#upload_errors")[0].innerHTML += err.file.name + " upload failed, because " + err.message + "<br>";
           $("#upload_errors").show();
        }
    }
});

uploader.init();

});
</script>
<?php } ?>
