<?php
/**
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title><?php echo $page_title;?> | <?php echo SITE_TITLE; ?></title>
    <meta name="description" content=""> 

    <meta name="robots" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().ASSETURI; ?>/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url().ASSETURI; ?>/fonts/opensans-fonts.css" type="text/css" />
    <style>
        .form-signin {
            max-width: 330px;
            padding: 15px;
            margin: 0 auto;
        }
        .form-signin .form-signin-heading,
        .form-signin .checkbox {
            margin-bottom: 10px;
            text-align: center;
        }
        .form-signin .checkbox {
            font-weight: normal;
        }
        .form-signin .form-control {
            position: relative;
            height: auto;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            padding: 10px;
            font-size: 16px;
        }
        .form-signin .form-control:focus {
            z-index: 2;
        }
        .form-signin input[type="email"] {
            margin-bottom: -1px;
            border-bottom-right-radius: 0;
            border-bottom-left-radius: 0;
        }
        .form-signin input[type="password"] {
            margin-bottom: 10px;
            border-top-left-radius: 0;
            border-top-right-radius: 0;
        }
    </style>

    <!--[if lt IE 9]><script src="<?php echo base_url().ASSETURI; ?>/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url().ASSETURI; ?>/html5shiv.js"></script>
      <script src="<?php echo base_url().ASSETURI; ?>/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
    <div class="container">

      <form class="form-signin" role="form" action="<?php echo site_url(AUTHOR_URI . '/auth');?>" method="post">
        <?php 
        $_auth_fail_msg = $this->session->flashdata('auth_fail_msg');
        if(!empty($_auth_fail_msg)) { 
        ?>
        <div class="alert alert-danger">
        <?php echo $_auth_fail_msg;?>
        </div>
        <?php } ?>
        <h2 class="form-signin-heading">MIE</h2>
        <input type="email" class="form-control" name="un" placeholder="Email address" required autofocus>
        <input type="password" class="form-control" name="up" placeholder="Password" required>
        <!--
		<label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
		-->
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
	<script src="<?php echo base_url().ASSETURI; ?>/jquery.min.js"></script>
    <script src="<?php echo base_url().ASSETURI; ?>/bootstrap.min.js"></script>
  </body>
</html>