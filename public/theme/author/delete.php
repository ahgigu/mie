<?php if ($_partial === 'content') { ?>
<div class="col-md-12">
    <ol class="breadcrumb">
        <li class="active">
                public
        </li>
        <li <?php echo isset($active_path[0])? '' : 'class="active"';?>>
            <?php if(isset($active_path[0])) { ?>
            <a href="<?php echo base_url(AUTHOR_URI);?>">
                content
            </a>
            <?php } else { ?>
            content
            <?php } ?>
        </li>
        <?php
            $_path_dirs = explode('/', $active_path);
            $_path_dirs = array_filter($_path_dirs);
            $_path_dirs = array_values($_path_dirs);
            $_count = count($_path_dirs);
            $_bread_path = '';
            foreach($_path_dirs as $key=>$_dir){
                if(isset($_dir[0])){
                    $_bread_path .=  '/' . $_dir;
                    if($_count == $key + 1){
        ?>
                    <li class="active">
                        <?php echo $_dir;?>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo site_url(AUTHOR_URI . '?path=') . urlencode($_bread_path);?>">
                            <?php echo $_dir;?>
                        </a>
                    </li>

        <?php }}} ?>
    </ol>
     <ul class="pager">
        <li class="previous">
            <a href="<?php echo site_url(AUTHOR_URI . (isset($active_path[0]) ? '?path='. urlencode($active_path) : ''));?>">
               &larr;  Return
            </a>
        </li>
    </ul>
    <div class="table-responsive">
    <form id="file_form" action="<?php echo site_url(AUTHOR_URI . '/do_delete'); ?>" method="post" accept-charset="utf-8">
        <input type="hidden" name="path" value="<?php echo $active_path; ?>">
        <table class="table table-striped">
            <tbody>
                <?php 
                if(!empty($file_names)) {
                foreach($file_names as $_file) {?>
                <tr>
                    <td width="5%">
                        <input type="checkbox" checked="true" name="name[]" value="<?php echo $_file; ?>">
                    </td>
                    <td>
                        <?php echo $_file; ?>
                    </td>
                    <td width="20%">
                    </td>
                    <td width="20%">
                    </td>
                </tr>
                <?php }} ?>
            </tbody>
        </table>
      <button type="submit" class="btn btn-danger">Delete</button>
    </form>
    </div>
</div>
<?php } ?>
