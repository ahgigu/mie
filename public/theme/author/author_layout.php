<?php
/**
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title><?php echo $page_title;?> | <?php echo SITE_TITLE; ?></title>
    <meta name="description" content=""> 

    <meta name="robots" content="">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().ASSETURI; ?>/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url().ASSETURI; ?>/fonts/opensans-fonts.css" type="text/css" />
    <link href="<?php echo base_url().ASSETURI; ?>/style.css" rel="stylesheet">

    <!--[if lt IE 9]><script src="<?php echo base_url().ASSETURI; ?>/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url().ASSETURI; ?>/html5shiv.js"></script>
      <script src="<?php echo base_url().ASSETURI; ?>/respond.min.js"></script>
    <![endif]-->
    <?php 
        $_vars['_partial']='head'; 
        echo $this->load->view($partial, $_vars, TRUE);
    ?>
  </head>

  <body>

    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">MIE</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">          
            <li class="active">
                <a href="<?php echo site_url(AUTHOR_URI); ?>">files</a>

            </li>

            <li>
                <a href="<?php echo site_url(AUTHOR_URI.'/download');?>">
                backup
                </a>
            </li>

          </ul>

          <ul class="nav navbar-nav navbar-right">
               <li>
                <a href="<?php echo site_url(AUTHOR_URI.'/logout');?>">
                logout
                </a>
            </li>
          </ul>

        </div>
      </div>
    </div>

    <div class="container">
        <div class="row">
        <?php 
            $_vars['_partial']='content'; 
            echo $this->load->view($partial, $_vars, TRUE);
        ?>
        </div>
    <hr>
    <footer>
        <p>© MIE was made by Liu Tao(<a href="http://ahgigu.com/" target="_blank">ahgigu</a>) with love.</p>
     </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url().ASSETURI; ?>/jquery.min.js"></script>
    <script src="<?php echo base_url().ASSETURI; ?>/bootstrap.min.js"></script>
    <?php 
        $_vars['_partial']='end'; 
        echo $this->load->view($partial, $_vars, TRUE);
    ?>
  </body>
</html>
