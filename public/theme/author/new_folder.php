<?php if ($_partial === 'content') { ?>
<div class="col-md-12">
    <ol class="breadcrumb">
        <li class="active">
                public
        </li>
        <li <?php echo isset($active_path[0])? '' : 'class="active"';?>>
            <?php if(isset($active_path[0])) { ?>
            <a href="<?php echo base_url(AUTHOR_URI);?>">
                content
            </a>
            <?php } else { ?>
            content
            <?php } ?>
        </li>
        <?php
            $_path_dirs = explode('/', $active_path);
            $_path_dirs = array_filter($_path_dirs);
            $_path_dirs = array_values($_path_dirs);
            $_count = count($_path_dirs);
            $_bread_path = '';
            foreach($_path_dirs as $key=>$_dir){
                if(isset($_dir[0])){
                    $_bread_path .=  '/' . $_dir;
                    if($_count == $key + 1){
        ?>
                    <li class="active">
                        <?php echo $_dir;?>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="<?php echo site_url(AUTHOR_URI . '?path=') . urlencode($_bread_path);?>">
                            <?php echo $_dir;?>
                        </a>
                    </li>

        <?php }}} ?>
    </ol>
<?php if(validation_errors()) { ?>
    <div class="alert alert-danger">
        <?php echo validation_errors(); ?>
    </div>
<?php } ?>

     <ul class="pager">
        <li class="previous">
            <a href="<?php echo site_url(AUTHOR_URI . (isset($active_path[0]) ? '?path='. urlencode($active_path) : ''));?>">
               &larr;  Return
            </a>
        </li>
    </ul>
    <form role="form" method="post" action="<?php echo site_url(AUTHOR_URI . '/do_new_folder'.(isset($active_path[0]) ? '?path='. urlencode($active_path) : ''));?>">
      <div class="form-group <?php echo form_error('name')?'has-error':''; ?>">
        <label for="name">Folder Name</label>
        <input type="text" class="form-control" name="name" id="name" value="<?php echo set_value('name',''); ?>" placeholder="Folder Name">
        <input type="hidden" name="path" value="<?php echo $active_path;?>">
      </div>
      <button type="submit" class="btn  btn-primary">Create</button>
    </form>
</div>
<?php } ?>
