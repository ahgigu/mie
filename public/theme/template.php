<?php
/**
 */
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="../../assets/ico/favicon.ico">
    <title><?php echo empty($meta['title']) ? '': $meta['title'].' | '; ?><?php echo $site_title; ?></title>
<?php if(!empty($meta['description'])) { ?>
    <meta name="description" content="<?php echo $meta['description']; ?>">
<?php } ?>
<?php if(!empty($meta['keywords'])) { ?>
    <meta name="keywords" content="<?php echo $meta['keywords']; ?>">
<?php } ?>
<?php if(!empty($meta['robots'])) { ?>
    <meta name="robots" content="<?php echo $meta['robots']; ?>">
<?php } ?>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url().ASSETURI; ?>/bootstrap.min.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="<?php echo base_url().ASSETURI; ?>/fonts/opensans-fonts.css" type="text/css" />
    <link href="<?php echo base_url().ASSETURI; ?>/style.css" rel="stylesheet">
    <!--[if lt IE 9]><script src="<?php echo base_url().ASSETURI; ?>/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo base_url().ASSETURI; ?>/html5shiv.js"></script>
      <script src="<?php echo base_url().ASSETURI; ?>/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo site_url(); ?>">MIE</a>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">          
          <?php foreach($packages as $package) { ?>
                <?php if (!empty($package['path'])) { ?> 
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $package['display']; ?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                        <?php foreach($pages as $page) { ?>
                            <?php if (($package['path'] == $page['package']) && ($front_path != $page['path'])) { ?> 
                                <li><a href="<?php echo $page['url']; ?>"><?php echo $page['title']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                        </ul>
                    </li>
                <?php } ?>
                <?php if (empty($package['path'])) { ?> 
                        <?php foreach($pages as $page) { ?>
                            <?php if (empty($page['package']) && ($page['url']!=site_url()) && ($front_path != $page['path'])) { ?>
                                 <?php if ($page['uri'] == $uri) { ?> 
                                <li class="active">
                                <?php } else { ?>
                                <li>
                                <?php } ?>
                                    <a href="<?php echo $page['url']; ?>"><?php echo $page['title']; ?></a>
                                </li>
                            <?php } ?>
                        <?php } ?>
                <?php } ?>
          <?php } ?>
          </ul>
        </div>
      </div>
    </div>

    <div class="container">
        <?php echo $content; ?>
        <ul class="pager">
            <?php if(!empty($prev_page['title'])) {  ?>
              <li class="previous">
                <a href="<?php echo ($prev_page['path'] === $front_path) ? site_url() : $prev_page['url']; ?>">&larr; <?php echo $prev_page['title']; ?></a></li>
            <?php } ?>
            <?php if(!empty($next_page['title'])) {  ?>
              <li class="next"><a href="<?php echo $next_page['url']; ?>"><?php echo $next_page['title']; ?> &rarr;</a></li>
            <?php } ?>
        </ul>
    <hr>
    <footer>
        <p>© MIE was made by Liu Tao(<a href="http://ahgigu.com/" target="_blank">ahgigu</a>) with love.</p>
     </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url().ASSETURI; ?>/jquery.min.js"></script>
    <script src="<?php echo base_url().ASSETURI; ?>/bootstrap.min.js"></script>
  </body>
</html>
