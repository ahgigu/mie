<?php
/**
 * MIE
 *
 * MIE is a markdown file CMS. It helps convert your flat files, which are written using Markdown, to beautiful web pages. 
 *
 * NOTICE OF LICENSE
 *
 * Licensed under the Open Software License version 3.0
 *
 * This source file is subject to the Open Software License (OSL 3.0) that is
 * bundled with this package in the files license.txt / license.rst.  It is
 * also available through the world wide web at this URL:
 * http://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to obtain it
 * through the world wide web, please send an email to
 * licensing@ellislab.com so we can send you a copy immediately.
 *
 * @package		MIE
 * @author		Liu Tao
 * @copyright	Copyright (c) 2014. (http://ahgigu.com/)
 * @license		http://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * @link		http://ahgigu.com
 * @since		Version 1.1
 * @filesource
 */

/*
 * --------------------------------------------------------------------
 * CUSTOMIZED CONFIG
 * --------------------------------------------------------------------
 *
 */
 date_default_timezone_set("Asia/Chongqing");
define('AUTHOR_URI', 'donttryit');

//The path to "ci" folder
define('CIDIR', 'ci'.DIRECTORY_SEPARATOR);

//The path to "content" folder
define('PUBLICDIR', 'public');
define('DBDIR', 'public/database');
define('CONTENTPATH', PUBLICDIR.'/content/');
define('CONTENTEXT', '.md');

//The path to "theme" folder
define('THEMEURI', PUBLICDIR . '/theme');
define('ASSETURI', THEMEURI . '/assets');

define('DATEFORMATE', 'jS M Y');

define('SITE_TITLE', 'Ahgigu is a web artisan');
define('SITE_DESC', 'Liu Tao(Ahgigu) loves PHP, Nginx, Linux, Git, Memcached, Redis, Open source database, Vim and everything that makes life easier and cool.');
define('SITE_KEYWORDS', 'PHP, Nginx, Linux, Git, MIE, Memcached');
/*
 * --------------------------------------------------------------------
 * LOAD THE BOOTSTRAP FILE
 * --------------------------------------------------------------------
 *
 * And away we go...
 */
require_once CIDIR . 'ci_index.php';

/* End of file index.php */
/* Location: ./index.php */
